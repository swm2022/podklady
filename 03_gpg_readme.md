# Create keys

## Create main key

Passphrase will be prompted by command.

```console
$ gpg --batch --quick-generate-key "Firstname Lastname <lastname@example.com>" rsa4096 sign,cert never
```

## Add signing and encryption subkeys

Passphrase will be prompted by commands.

```
$ FPR=$(gpg --list-options show-only-fpr-mbox --list-secret-keys | awk '{print $1}')
$ gpg --batch --quick-add-key $FPR rsa4096 sign never
$ gpg --batch --quick-add-key $FPR rsa4096 encrypt never
```

## List secret keys

```console
$ gpg --list-secret-keys --keyid-format LONG
/home/hrb33/.gnupg/pubring.kbx
------------------------------
sec   rsa4096/6B2FBB2157659932 2022-01-04 [SC]
      DA606F9C46F97A9B80AC5BD66B2FBB2157659932
uid             [  absolutní ] David Hrbáč <david@hrbac.cz>
ssb   rsa4096/4B2F8D01636F5693 2022-01-04 [E]
ssb   rsa4096/70B0E7BB87B8CFEC 2022-01-04 [S]

```

## Export public encrypt subkey

Export just the subkey (! encrypt)

```console
$ SSE=$(gpg --list-secret-keys --with-colons | awk -F: '($1 == "ssb") && ($12 == "e") {print $5}')
$ gpg --armor --export $SSE! > lastname.txt
```

## Export public sign subkey and add to gitlab

Export just the subkey (! sign)

```console
$ SSS=$(gpg --list-secret-keys --with-colons | awk -F: '($1 == "ssb") && ($12 == "s") {print $5}')
$ gpg --armor --export $SSS!
```

1. In the top-right corner, select your avatar.
2. Select Edit profile.
3. On the left sidebar, select GPG Keys.
4. Paste your public key in the Key text box. 
5. Select Add key to add it to GitLab. You can see the key’s fingerprint, the corresponding email address, and creation date.

## Git

```console
git config --global user.signingKey $SSS
git config --global gpg.program gpg
git config --global commit.gpgsign true
```
