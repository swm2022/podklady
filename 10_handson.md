# Gitlab hands on (10th lecture)

## Prerequisities Windows WSL

1. Install Desktop Docker
1. Enable WSL support during the installation.
1. Run Desktop Docker
1. Open Ubuntu terminal

## Prerequisities Linux

1. Install docker.io `sudo apt update && sudo apt install docker.io`

## Prerequisities Linux & WSL

Add yourself to docker group so you can run `docker` without `sudo`. There are two ways:
1. `sudo usermod -aG docker $USER`
1. Modify `/etc/group` and append your account to group docker.

## Task 1 - Simple Docker workflow

1. Try to run container **docker/getting-started** `docker run -d -p 80:80 docker/getting-started`.
1. Try openning browser at http://localhost
1. Inspect running containers `docker ps -a`
1. Find proper **CONTAINER_ID** of the container.
1. Run with proper **CONTAINER_ID** `docker logs CONTAINER_ID -f` to see the logs while browsing the getting started site.
1. Stop the container with `docker stop CONTAINER?ID`.
1. Inspect container status `docker ps -a` or `docker ps`.
1. Try removing container with `docker rm CONTAINER_ID`.
1. Inspect container status `docker ps -a` or `docker ps`.
1. Run `docker images` to inspect the images.
1. Find proper **IMAGE_ID**.
1. Try removing **docker/getting-started** image with `docker rmi IMAGE_ID`.
1. Run `docker images` to inspect the images.

## Task 2 - Create a simple container

1. Create empty folder
1. Find Python containers with `docker search python`
1. Inspect tags on Docker Hub https://hub.docker.com/_/python
1. Create simple container based on Python 3.8 image.
1. Create Dockerfile:
   ```docker
   FROM python:3.8
   ```
1. Build a contaner out of it `docker build -t mytest:3.8 .`
1. Inspect images with `docker images`
1. Try running container with `docker run mytest:3.8`. What happend?
1. Inspect container status `docker ps -a` or `docker ps`.
1. Try running container with `docker run mytest:3.8 python -V`.

----

1. Modify Dockerfile to:
   ```docker
   FROM python:3.9
   ```
1. Build a contaner out of it `docker build -t mytest:3.9 .`
1. Inspect images with `docker images`
1. Try running container with `docker run mytest:3.9`. What happend?
1. Inspect container status `docker ps -a` or `docker ps`.
1. Try running container with `docker run mytest:3.9 python -V`.
1. Run `docker history mytest:3.9`

----

1. Modify Dockerfile to:
   ```docker
   FROM python:3.9

   RUN apt-get update \
       && apt-get install -y --no-install-recommends \
           postgresql-client \
       && rm -rf /var/lib/apt/lists/*
   ```
1. Build a contaner out of it `docker build -t mytest:3.9 .`
1. Inspect images with `docker images`
1. Try running container with `docker run mytest:3.9`. What happend?
1. Inspect container status `docker ps -a` or `docker ps`.
1. Try running container with `docker run mytest:3.9 python -V`.
1. Run `docker history mytest:3.9`. What's changed?

----

1. Modify Dockerfile to:
   ```docker
   FROM python:3.9

   RUN apt-get update \
       && apt-get install -y --no-install-recommends \
           postgresql-client \
       && rm -rf /var/lib/apt/lists/*

   RUN pip install mkdocs mkdocs-material
   ```
1. Build a contaner out of it `docker build -t mytest:3.9 .`
1. Inspect images with `docker images`
1. Try running container with `docker run mytest:3.9`. What happend?
1. Inspect container status `docker ps -a` or `docker ps`.
1. Try running container with `docker run mytest:3.9 python -V`.
1. Run `docker history mytest:3.9`. What's changed?
1. Run `docker run mytest:3.9 mkdocs -V`. Is `mkdocs` available?

----

1. Modify Dockerfile to:
   ```docker
   FROM python:3.9-alpine

   RUN pip install mkdocs mkdocs-material
   ```
1. Build a contaner out of it `docker build -t mytest:3.9-alpine .`
1. Inspect images with `docker images` and **compare** images sizes.
1. Try running container with `docker run mytest:3.9-alpine`. What happend?
1. Inspect container status `docker ps -a` or `docker ps`.
1. Try running container with `docker run mytest:3.9-alpine python -V`.
1. Run `docker history mytest:3.9-alpine`. What's changed?
1. Run `docker run mytest:3.9-alpine mkdocs -V`. Is `mkdocs` available?

## Docker HUB (optional)

*My account is **daviddhrbac** at Docker HUB, so I will use it as a sample within the commands.*

1. Create account at Docker HUB https://hub.docker.com/signup
1. Build a container out of the laster versions of your Docker file `docker build -t davidhrbac/mytest:3.9-alpine .`
1. Push it to Docker HUB `docker push davidhrbac/mytest:3.9-alpine`
1. Inspect repo at https://hub.docker.com/r/davidhrbac/mytest
1. Inspect images with `docker images`
1. Remove local copy with `docker rmi davidhrbac/mytest:3.9-alpine`.
1. Try pulling and running like `docker run davidhrbac/mytest:3.9-alpine mkdocs -V`

## Gitlab container registry

1. Create an empty public repo/project e.g. mkdocs-container in Gitlab.
1. Create a new **Dockerfile** within Web IDE and use **Python** template.
1. Remove lines so it reads like this:
   ```docker
   # This file is a template, and might need editing before it works on your project.
   FROM python:3.6

   # Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
   # Or delete entirely if not needed.
   RUN apt-get update \
       && apt-get install -y --no-install-recommends \
           postgresql-client \
       && rm -rf /var/lib/apt/lists/*
   ```
1. Save and commit
1. Create a new **.gitlab-ci.yml** within Web IDE and use **Docker** template.
1. Keep it, save and commit to master/main branch.
1. Inspect CI/CD of the project. Pipline should be running now.
1. Once finished - inspect project Container registry - you should see a new containter.

----

1. Clone repository to your computer.
1. Modify **.gitlab-ci.yml** line with `docker login` to:
   ```yml
   - echo $CI_BUILD_TOKEN | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
   ```
1. Save, commit and push.
1. Inspect CI/CD job.

----

1. Create a new branch 3.8.
1. Modify Python to `python:3.8` in **Dockerfile**.
1. Commit and push.
1. Inspect CI/CD and Container registry. What is the tag?

----

1. Find `$CI_COMMIT_REF_SLUG` ind **.gitlab-ci.yml** and replace with `$CI_COMMIT_REF_NAME`.
1. Commit and push.
1. Inspect CI/CD and Container registry. What is the tag?

----

1. Checkout to master branch.
1. Create a new branch 3.9
1. Modify Python to `python:3.9` in **Dockerfile**.
1. Commit and push. `git push --all` might be handy...
1. Inspect CI/CD and Container registry. What are the tags?

----

1. Cherry pick the $CI_COMMIT_REF_NAME adn commit to branch 3.9 `git cherry-pick HASH`.
1. Push
1. Inspect CI/CD and Container registry. What are the tags?

## Optional task A

1. Modify Dockerfile to:
   ```docker
   FROM python:3.9

   RUN apt-get update \
       && apt-get install -y --no-install-recommends \
           postgresql-client \
       && rm -rf /var/lib/apt/lists/*

   RUN pip install mkdocs mkdocs-material
   ```
1. Commit and push.
1. Inspect CI/CD and Container registry. What is the tag?
1. Try running image with `docker run URL mkdocs -V`

## Optional task B

1. Modify your MKDocs projet from previous hands on to use this new container for you.
