# Hands on

## Remaining from previous hands on (3rd lecture)

1. Create an empty project in your Gitlab namespace with README.md. (Hint: Make sure it's a public one and initialised with a README.)
   1. Clone your new repository to your computer `/tmp/_give_name_`.
   1. Are there any commits in the repository yet?
   1. What are the 'remotes' of your Git repository?
   1. Modify README.md, commit, and push.
   1. What has happend on Gitlab project view?
1. Create empty project in your Github namespace. (Hint: Public one, no content at all.)
   1. Go to your local copy of Gitlab project `/tmp/_give_name_` and create a new remote called `gh`. (Hint:`git remote ...`, might be described a new Github repositry...)
   1. Try to push a copy to `gh` remote

## Gitlab hands on (5th lecture)

1. Create a new project based on GL pages plain HTML template.
   1. Clone project
   1. Modify `index.html` and push the project.
   1. Inspect CI (continuous integrity) and the content.
   1. Go to project setting - inspect pages setting.
   1. Make sure SSL certificate is enabled.
   1. Open the site in a browser.
   1. Checked being OK remove the project permanetly.
1. Create a new project based on GL pages Jekyll template.
   1. Go to project CI and hit button `run pipeline`.
   1. Inspect CI (continuous integrity) and the content.
   1. Go to project setting - inspect pages setting.
   1. Make sure SSL certificate is enabled.
   1. Open the site in a browser.
   1. Clone project.
   1. Modify `_config.yml`, correct URL setting and push the project.
   1. Open the site in a browser.
   1. Modify page About
      1. Change page tile.
      1. Change page permalink. (Hint: e.g. `permalink: /about-me/`)
      1. Commit, push, and inspect CI.
      1. Go to browser and check link and page title.
   1. Go to project setting - inspect pages setting.
      1. Try adding custom domain (Hint:You can use own or you will be provided. david@hrbac.cz)
      1. Verificate domain.
      1. Inspect site in a browser.
      1. Modify `_config.yml`, correct URL setting and push the project.
      1. Inspect site in a browser.
   1. Checked being OK remove the project permanetly.


## Gitlab hands on (optional)
1. Create a new ticket and try adding special GL references. (https://docs.gitlab.com/ee/user/markdown.html#gitlab-specific-references)
   1. issue
   1. issue from different project
   1. commit, MR (also from different project)
   1. user (Hint: Do not use groups, all the members get notification...)
1. Visit https://docs.gitlab.com/ee/user/project/description_templates.html
   1. Read the document.
   1. Try to create a new issue template with task list.
   1. Commit and create a new ticket based on the template.
