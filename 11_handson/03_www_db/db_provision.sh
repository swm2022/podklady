#!/bin/bash
#provision DBBox Version 0.0.0
# Edited by Jessica Rankins 4/17/2017


rm -f postinstall.sh

apt-get update
# mysql username: root
# mysql password: rootpass
debconf-set-selections <<< 'mysql-server mysql-server/root_password password rootpass'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password rootpass'
apt install -y mysql-client mysql-server mysql-common bash-completion
#  
#  sed -i "s/bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" "/etc/mysql/my.cnf"
sed -i "s/bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf
#  
#  # Allow root access from any host
echo "CREATE USER 'root'@'%' IDENTIFIED BY 'rootpass'; GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;" | mysql -u root --password=rootpass
systemctl restart mysql
#  #echo "GRANT PROXY ON ''@'' TO 'root'@'%' WITH GRANT OPTION" | mysql -u root --password=rootpass
#  sudo service mysql restart
#  
#  # Create database for form responses (WebExampleBox)
#  mysql -uroot -p'rootpass' -e "DROP DATABASE IF EXISTS formresponses; 
#  	CREATE DATABASE formresponses; 
#  	USE formresponses; 
#  	CREATE TABLE response (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
#  		firstname VARCHAR(20), lastname VARCHAR(20), 
#  		email VARCHAR(50), submitdate DATETIME);"
#  sudo service mysql restart
#  
echo cd / >> /home/vagrant/.bashrc
echo "Hello World from DB!"
