# Hands on

## Remaining from previous hands on (5th lecture)

1. Visit https://docs.gitlab.com/ee/user/project/description_templates.html
   1. Read the document.
   1. Try to create a new issue template with a task list.
   1. Commit and create a new ticket based on the template.

## Gitlab hands on (6th lecture)

1. Prepare `mkdocs` working environment
   1. Install python virtualenv `sudo apt update; sudo apt install python3-virtualenv`.
   1. Create a folder, enter the folder `mkdir mydocs; cd mydocs`.
   1. Create a virtual environment for python `virtualenv venv` or `virtualenv venv -p python3`.
   1. Activate virtual environment `source venv/bin/activate`.
   1. Check you have Python 3.x `python -V`.
   1. Get list of Python modules `pip list`.
   1. Install MKDocs `pip install mkdocs`.
   1. Check mkdocs available options with `mkdocs -h`.
1. Create a new documentation site `mkdocs new .`.
   1. Inspect folder content.
   1. Initialise GIT repository.
   1. Create `.gitignore` and add `venv/` folder.
   1. Create initial commit.
1. Run the server `mkdocs serve`.
   1. Open site in browser.
   1. Modify `index.md` to see the changes are propagated imidiatelly.
1. Install Material Design plugin `pip install mkdocs-material`.
   1. Enable material design in mkdocs `theme -> name` keywords in `mkdocs.yml`.
   1. Enable `language`, `font` and `features` settings in the theme.
   1. Add plugins section, inspect plugins at https://squidfunk.github.io/mkdocs-material/setup/setting-up-site-search/#built-in-search-plugin
   1. Add Markdown extensions, inspect extensions at https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/ and https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/
1. Try adding another page(s) `nav` section.
1. (OPTIONAL) Inspect minify plugin `pip install mkdocs-minify-plugin`, see https://github.com/byrnereese/mkdocs-minify-plugin
1. (OPTIONAL) Inspect PDF export plugin `pip install mkdocs-with-pdf`, see https://github.com/orzih/mkdocs-with-pdf

### mkdocs.yml

```yaml
site_name: My Documentation Guide
site_author: Myname Mysurname
site_description: No desc

# Copyright
copyright: Copyright &copy; 2022

# Repository
repo_name: myreponame
repo_url: https://gitlab.com/myreponame

theme:
  name: material
  language: en
  font:
    text: Roboto
    code: Roboto Mono
  features:
    - navigation.sections
    - navigation.tabs

plugins:
  - search
  #- minify:
  #    minify_html: true
  # - with-pdf:
markdown_extensions:
  - abbr
  - admonition
  - attr_list
  - toc:
      permalink: true
  - pymdownx.critic:
      mode: view
  - pymdownx.highlight
  - pymdownx.inlinehilite
  - pymdownx.superfences
  - pymdownx.snippets

# Page tree
nav:
  - General: index.md
  - Help: help.md

```

```yaml
# Page tree
nav:
  - General: index.md
  - Help:
    - Nav1: 1.md
    - Nav2: 2.md
    - Nav3: help/3.md
```
### CI/CD 1

Example `.gitlab-ci.yml`:

```yaml
pages:
  image: python:3.8
  before_script:
    - date
    - export TZ=Europe/Prague
    - date
    - python -V
    - pip install mkdocs-material mkdocs-redirects mkdocs-minify-plugin mkdocs-git-revision-date-localized-plugin
  script:
    - mkdocs build --site-dir public
  artifacts:
    paths:
    - public
```

1. Create CI/CD pipeline.
   1. Create `.gitlab-ci.yml`.
   1. Commit the state.
   1. Create a project in Gitlab.
   1. Add remote to the working GIT repository.
   1. Push the repo.
   1. Inspect CI/CD.
   1. Check log of CI job and find correlations with `.gitlab-ci.yml` steps.
   1. Make sure GL pages are working.
   1. Run pipeline again and compare elapsed time.

### CI/CD 2

Extended `.gitlab-ci.yml`:

```yaml
pages:
  image: python:3.8
  before_script:
    - python -V
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - pip install -r requirements.txt
    - mkdocs --version
    - pwd
  script:
    - mkdocs build
  artifacts:
    paths:
    - site
    expire_in: 1 week
  cache:
    paths:
      - .cache/pip
      - venv
```

1. Enable caching to speed up the CI pipeline
   1. Modify `.gitlab-ci.yml`.
   1. Create `requirements.txt` file with proper packages `pip freeze | grep mkdoc > requirements.txt`.
   1. Commit `requirements.txt` and `.gitlab-ci.yml`.
   1. Push the repo.
   1. Inspect CI/CD.
   1. Check log of CI job and find correlations with `.gitlab-ci.yml` steps.
   1. Make sure GL pages are working.
   1. Run pipeline again and compare elapsed time.

Requirements.txt sample:

```
mkdocs==1.2.3
mkdocs-material==8.2.5
mkdocs-material-extensions==1.0.3
```

## Gitlab hands on (optional)

Browse artifact of pages job with different artifacts path in `.gitlab-ci.yml`. What's the diffrence?

## VENV BASH integration

Add lines to `~/.bashrc` to modify `cd` behaviour:

```bash
function cd() {
  builtin cd "$@"

  if [[ -z "$VIRTUAL_ENV" ]] ; then
    ## If env folder is found then activate the vitualenv
      if [[ -d ./venv ]] ; then
        source ./venv/bin/activate
      fi
  else
    ## check the current folder belong to earlier VIRTUAL_ENV folder
    # if yes then do nothing
    # else deactivate
      parentdir="$(dirname "$VIRTUAL_ENV")"
      if [[ "$PWD"/ != "$parentdir"/* ]] ; then
        deactivate
      fi
  fi
}
```

## Gitlab hands on (7th lecture)

1. Create CI 1 and CI 2 from previous lesson.
1. Extend the CI with the following steps:
   1. Introduce a step which will modify footer with the built date.
      1. Create folder `scripts`.
      1. Create a BASH script to replace string.
      1. Do not forget to add execute bit to the script.
      1. Add it to pages step.
   1. Remove that version script line and make it a separate CI job.
   1. Extend the CI with Markdown lint.
   1. Extend the CI with post deploy script to check external links.


```bash
$ tree  -L 1
.
├── docs.it4i
├── material
├── mkdocs.yml
├── package.json
├── pathcheck.sh
├── README.md
├── requirements.txt
├── scripts
├── site
├── snippets
└── venv
```

add_version.sh

```bash
#!/bin/bash
VER=$(git log --pretty=format:'/ ver. %h / %ai' -n 1)
sed "s,__VERSION__, $VER," mkdocs.yml -i
YEAR=$(date +"%Y")
sed "s,__YEAR__, $YEAR," mkdocs.yml -i
```

MKDocs config:

```yaml
copyright: Copyright (c) 2013-__YEAR__ IT4Innovations__VERSION__
```

```yaml
  - bash scripts/add_version.sh
```

```yaml
capitalize:
  stage: test
  image: it4innovations/docker-mkdocscheck:latest
  script:
  - find mkdocs.yml docs.it4i/ \( -name '*.md' -o -name '*.yml' \) -print0 | xargs -0 -n1 scripts/titlemd.py --test
```

```yaml
docs:
  stage: test
  image: it4innovations/docker-mdcheck:latest
  allow_failure: true
  script:
  - mdl -r ~MD013,~MD010,~MD014,~MD024,~MD026,~MD029,~MD033,~MD036,~MD037,~MD046 *.md docs.it4i # BUGS
```

```yaml
ext_links:
  stage: after_test
  image: it4innovations/docker-mdcheck:latest
  allow_failure: true
  after_script:
  # remove JSON results
  - rm *.json
  script:
  - find docs.it4i/ -name '*.md' -exec grep --color -l http {} + | xargs awesome_bot -t 10 --allow-dupe --allow-redirect
  only:
  - master
```

