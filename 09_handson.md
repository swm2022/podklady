# Gitlab hands on (9th lecture)

Create a GIT repository with Python code and unit tests. Files are in 09_handson folder.

1. Create a repo
1. Create a virtual environment for python `virtualenv venv` or `virtualenv venv -p python3`.
1. Activate virtual environment `source venv/bin/activate`.
1. Introduce proper `.gitignore` file.
1. Fill it with Pythons files. (Move to scr folder).
   ```console
   https://gitlab.com/-/snippets/2284564
   https://gitlab.com/-/snippets/2284559
   ```
1. Try running `pytest` in current folder.
1. Try to find how to run coverage with `pytest-cov`
1. Try to find how to create HTML report for Pytest and coverage. (https://pytest-cov.readthedocs.io/en/latest/ and https://pytest-html.readthedocs.io/en/latest/)
1. Create a CI with 2 tasks:
   1. Task to create HTML report of unittest and code coverage.
      1. You need to install `pytest-cov` and `pytest-html`.
      1. You need to find out how to create reports to `public` folder. You want to make a Gitlab pages out of it!
   1. Task to get XML report of code coverage and introduce proper `coverage` regex.
      1. Make use of code coverage report:
      ```console
      coverage run -m pytest
      coverage report
      coverage xml
      ```
      1. Create regex for use in `coverage` directive in `.gitlab-ci.yml`. (https://docs.gitlab.com/ee/ci/yaml/index.html#coverage and https://regex101.com/r/gpVjKW/1)
      1. Create XML coverage report file for `cobertura` (https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html#python-example)
1. Once having CI up and running create an issue template with the following content:
   (Create folder `.gitlab/issue_templates/` see https://docs.gitlab.com/ee/user/project/description_templates.html#create-an-issue-template)
   ```markdown
   * [ ] Verify Gitlab pages link to unittest HTML report.
   * [ ] Verify Gitlab pages link to code coverage HTML report.
   * [ ] Create a new brach and modify code with a new method, e.g.:
         ```python
         def deactivate(self):
            self.profile['active'] = False
         ```
   * [ ] Verify the new MR is having code coverage percentage report.
   * [ ] Create README.md and add project badges to it:
     * [ ] CI status
     * [ ] Code coverage status
   ```
1. Create ticket based on the template.
1. Go over the tasks in the ticket and proceed the steps.
