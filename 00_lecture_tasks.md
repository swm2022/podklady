# Závěrečné práce

Verze: 0.9 (2022-04-03)

## "Malá" práce

Připravte repozitář na Gitlabu, který bude používat generátor statických stránek ve spojení s kontinuální integrací (CI). CI bude generovat HTML stránky z Markdown podkladu. Součástí CI by měl být také test kvality Markdown souborů. Práce vychází z [cvičení č. 7](07_handson.md#gitlab-hands-on-6th-lecture), kde jsou uvedeny i případné vhodné kontejnery.

Požadavky na kvalitu práce (hodnotící kritéria):

* [ ] GIT repozitář na Gitlabu.
  * [ ] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
* [ ] Použití libovolného generátoru statických stránek dle vlastního výběru. (MKdocs, Middleman, Jekyll, apod.)
* [ ] Vytvořená CI v repozitáři.
* [ ] CI má minimálně dvě úlohy:
  * [ ] Test kvality Markdown stránek.
  * [ ] Generování HTML stránek z Markdown zdrojů.
* [ ] CI má automatickou úlohou nasazení web stránek (deploy).
* [ ] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:

Přehled generátorů: https://jamstack.org/generators/

## Závěrečná práce

Závěrečná práce vychází z "malé" práce. Použijte svůj projekt, na kterém pracujete a dejte jej do GIT repozitáře a pokuste se připravit kontinuální integraci svého projektu. V závislosti na použitém programovacím jazyce či sadě programovacích jazyků navhrněte a vytvořte vhodné úkoly ve vaší kontinuální integraci. Pro úlohy v CI použijte vhodné Docker kontejnery, případně vytvořte vlastní. Pokud povaha CI vyžaduje, rozdělte úlohy (jobs) do vhodných fází (stages).

Požadavky na kvalitu práce (hodnotící kritéria):
* [ ] GIT repozitář na Gitlabu.
  * [ ] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
* [ ] Použití vlastního projektu (ideální kandidát má kombinaci jazyků - např. Python a BASH, Markdown a JS, apod.)
* [ ] Vytvořená CI v repozitáři.
* [ ] CI má minimálně tři úlohy:
  * [ ] Test kvality Markdown stránek. (Kvalita dokumentace je důležitá. Minimálně README.md by mělo být v repozitáři dostupné.)
  * [ ] Kontrola syntaxe každého jazyka.
  * [ ] Použití "lint" nástroje pro každý jazyk.
* [ ] (Volitelné/doporučené) Nástroj na kontrolu dostupnosti aktualizací/security updates pro moduly jazyka (pokud existuje).
* [ ] (Volitelné/doporučené) Nástroj na kontrolu testů/code coverage (pokud existuje).
* [ ] (Volitelné/doporučené) Pokud některé nástroje umí generovat HTML reporty - umistěte je na Gitlab pages.
* [ ] (Volitelné/doporučené) Repozitář obsahuje šablonu ticketu.
